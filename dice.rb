require_relative 'player'

def play_game(players)

  turn_count = 1

  # Keep Playing till game mode reaches final
  while Player.game_mode != :final

    case Player.game_mode
      when :start
        puts "Turn #{turn_count}:"
      when :last_turn
        puts "Final Round:"
        Player.set_final_round
    end
    puts "--------"

    for i in 0...players.length
      agg_total = players[i].total_score
      next if Player.game_mode == :final && agg_total >= Player::PLAYER_POINTS_NEEDED_TO_TRANSITION_GAME_MODE
      current_total = 0
      no_of_dices = Player::DEFAULT_DICE_COUNT

      loop do
        puts ""
        current_roll = players[i].roll(no_of_dices)
        score, no_dices_unused = Player.calc_score(current_roll)
        current_total = current_total + score
        # Too bad, can't really move forward now.
        current_total = 0 if score == 0

        puts "Score in this round: #{current_total}"
        puts "Total Score: #{agg_total + current_total}"

        # Can move forward
        if Player.can_roll?(score, no_of_dices, no_dices_unused)
          break if !players[i].continue_next_roll?(no_dices_unused)
        else
          break
        end
        no_of_dices = (no_dices_unused == 0) ? Player::DEFAULT_DICE_COUNT : no_dices_unused
      end
      players[i].add_score_to_total(current_total)
    end
    turn_count = turn_count + 1
  end

  top_score = 0
  top_score_index = -1
  for i in 0...players.length
    if players[i].total_score > top_score
      top_score_index = i
      top_score = players[i].total_score
    end
  end

  puts "Player #{players[top_score_index].name} wins with top score:#{top_score}"
end

def setup_game(no_of_players)
  players = []
  for i in 1..no_of_players do
    players << Player.new(i.to_s)
  end
  players
end

puts "Enter number of players: "
players_str = gets.chomp
begin
  no_of_players = Integer(players_str)
  raise "Please enter a valid positive integer" if no_of_players <= 0
rescue => e
  puts "Your Input #{players_str} raised an Exception #{e.message}. Please enter a valid integer"
  exit
end
players = setup_game(no_of_players)
play_game(players)