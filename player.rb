require_relative 'input_error'

class Player
  @@game_mode
  attr_reader :total_score, :name

  # Game Mode states
  GAME_MODE = [:start, :last_turn, :final]

  PLAYER_POINTS_NEEDED_TO_TRANSITION_GAME_MODE = 3000
  PLAYER_POINTS_NEEDED_TO_TRANSITION_PLAYER_MODE = 300
  DEFAULT_DICE_COUNT = 5

  # Player mode states
  PLAYER_MODE = [:start, :in_game, :final]

  def self.game_mode
    @game_mode ||= :start
  end

  def self.set_final_round
    @game_mode = :final
  end

  def self.game_transition(score)
    if @game_mode == :start && score >= PLAYER_POINTS_NEEDED_TO_TRANSITION_GAME_MODE
      @game_mode = :last_turn
    end
  end

  def self.validate_and_return_rolls(input_roll, dice_count)
    rolls = input_roll.split(",")
    raise RollInputError, "Roll should consist of #{dice_count} dices(comman separated)" if rolls.length != dice_count
    final_rolls = []
    for i in 0...rolls.length
      begin
        r = Integer(rolls[i])
        raise RollInputError, "Dice values should be an integer between 1 and 6 (inclusive)" if !r.between?(1,6)
        final_rolls.push r
      rescue ArgumentError => e
        raise RollInputError, "Dice values should be an integer between 1 and 6 (inclusive)"
      end
    end
    final_rolls
  end

  def self.can_roll?(score, current_dices_count, unused_dices_count)
    return true if score > 0 && (unused_dices_count > 0 || (unused_dices_count == 0 && current_dices_count == Player::DEFAULT_DICE_COUNT))
    false
  end


  def self.calc_score(roll)
    score = 0

    # Hash of dice number to count of each
    dice_number = {}
    roll.each do |each_roll|
      if dice_number[each_roll].nil?
        dice_number[each_roll] = 1
      else
        dice_number[each_roll] = dice_number[each_roll] + 1
      end
    end

    unused = 0
    dice_number.each do |dice_number, count|
      remaining_count = count
      if count >= 3
        case dice_number
          when 1
            score = score + 1000
          else
            score = score + (dice_number * 100)
        end
        remaining_count = remaining_count - 3
      end

      case dice_number
        when 1,5
          score = score + (remaining_count * 100)
        else
          unused = unused + remaining_count
      end
    end
    return score, unused
  end

  def initialize(name)
    @name = name
    @total_score = 0
    @player_mode = :start
  end

  def roll(no_of_dices)
    correct_input = false
    final_roll = []
    loop do
      puts "Player #{name} rolls: "
      begin
        roll = gets.chomp
        final_roll = Player.validate_and_return_rolls(roll, no_of_dices)
        correct_input = true
      rescue RollInputError => e
        puts "Exception in getting roll input: #{e.message}"
      end
      break if correct_input
    end
    final_roll
  end

  def continue_next_roll?(no_dices_unused)
    msg = "Do you want to roll "
    case no_dices_unused
      when 0
        msg += "all #{Player::DEFAULT_DICE_COUNT} dices?"
      when 1
        msg += "#{no_dices_unused} dice?"
      else
        msg += "#{no_dices_unused} dices?"
    end
    puts "#{msg} (y/n):"

    continue_next = false
    # Making sure user is inputting correctly if he wants to roll again
    user_input = nil
    loop do
      user_input = gets.chomp
      user_input.downcase!
      case user_input
        when "y"
          continue_next = true
          break
        when "n"
          break
        else
          puts "Please enter either y/n"
      end
    end
    continue_next
  end


  def add_score_to_total(score)
    case @player_mode
      when :start
        if score >= PLAYER_POINTS_NEEDED_TO_TRANSITION_PLAYER_MODE
          @player_mode = :in_game
          @total_score = @total_score + score
        end
      else
        @total_score = @total_score + score
    end
    Player.game_transition(@total_score)
  end

end